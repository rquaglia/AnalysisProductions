###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Read an HLT2 file and create an ntuple with the new DaVinci configuration.
"""
import Functors as F
import FunTuple.functorcollections as FC
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from PyConf.reading import get_particles, get_pvs, get_rec_summary
from DaVinci.algorithms import create_lines_filter
from DaVinciMCTools import MCTruthAndBkgCat
from FunTuple.functorcollections import MCHierarchy, MCPrimaries, MCPromptDecay, Kinematics, SelectionInfo, HltTisTos, MCVertexInfo, MCKinematics, ParticleID, EventInfo
from PyConf.reading import get_odin  # get_decreports,
from DecayTreeFitter import DecayTreeFitter

_basic = "basic"
_composite = "composite"
_toplevel = "toplevel"

def all_variables(pvs, mctruth, ptype, candidates=None, ftAlg=None):
    """
    function that returns dictionary of functors that work.
    
    functors are listed in order of https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/selection/thor_functors_reference.html#module-Functo
    """
    if ptype not in [_basic, _composite]:
        Exception(f"I want {_basic} or {_composite}. Got {ptype}")
    all_vars = FunctorCollection({})
    
    comp = _composite == ptype or _toplevel == ptype  # is composite
    basic = _basic == ptype  # is not composite
    top = _toplevel == ptype  # the B

    # First import everything that comes in functorcollections
    all_vars += FC.Kinematics()
    if basic:
        all_vars += FC.ParticleID(extra_info=True)
    
    if comp:
        all_vars.update({"ALV": F.ALV(Child1=1, Child2=2)})
        
    if comp:  # all these require a vertex
        all_vars.update({"BPVCORRM": F.BPVCORRM(pvs)})
        all_vars.update({"BPVCORRMERR": F.BPVCORRMERR(pvs)})
        all_vars.update({"BPVDIRA": F.BPVDIRA(pvs)})
        all_vars.update({"BPVDLS": F.BPVDLS(pvs)})
        all_vars.update({"BPVETA": F.BPVETA(pvs)})
        all_vars.update({"BPVFD": F.BPVFD(pvs)})
        all_vars.update({"BPVFDCHI2": F.BPVFDCHI2(pvs)})
        all_vars.update({"BPVFDIR": F.BPVFDIR(pvs)})
        all_vars.update({"BPVFDVEC": F.BPVFDVEC(pvs)})

    all_vars.update({"BPVIP": F.BPVIP(pvs)})
    all_vars.update({"BPVIPCHI2": F.BPVIPCHI2(pvs)})
    all_vars.update({"BPVX": F.BPVX(pvs)})
    all_vars.update({"BPVY": F.BPVY(pvs)})
    all_vars.update({"BPVZ": F.BPVZ(pvs)})

    if comp:  # all these require a vertex
        all_vars.update({"ALLPV_FD": F.ALLPV_FD(pvs)})
        all_vars.update({"ALLPV_IP": F.ALLPV_IP(pvs)})
        all_vars.update({"BPVLTIME": F.BPVLTIME(pvs)})
        all_vars.update({"BPVVDRHO": F.BPVVDRHO(pvs)})
        all_vars.update({"BPVVDX": F.BPVVDX(pvs)})
        all_vars.update({"BPVVDY": F.BPVVDY(pvs)})
        all_vars.update({"BPVVDZ": F.BPVVDZ(pvs)})
    
    all_vars.update({"CHARGE": F.CHARGE})
    all_vars.update({"CHI2": F.CHI2})
    all_vars.update({"CHI2DOF": F.CHI2DOF})

    if top:  # apply this only to B
        all_vars.update({"CHILD1_PT": F.CHILD(1, F.PT)})  # example of CHILD
        all_vars.update({"Ds_END_VZ": F.CHILD(1, F.END_VZ)})
        all_vars.update({"Delta_END_VZ_DsB0": F.CHILD(1, F.END_VZ) - F.END_VZ})
    
    if comp:
        all_vars.update({"DOCA": F.SDOCA(Child1=1, Child2=2)})
        all_vars.update({"DOCACHI2": F.SDOCACHI2(Child1=1, Child2=2)})
        all_vars.update({"END_VRHO": F.END_VRHO})
        all_vars.update({"END_VX": F.END_VX})
        all_vars.update({"END_VY": F.END_VY})
        all_vars.update({"END_VZ": F.END_VZ})
    
    all_vars.update({"ETA": F.ETA})
    all_vars.update({"FOURMOMENTUM": F.FOURMOMENTUM})
    all_vars.update({"ISBASIC": F.ISBASICPARTICLE})

    if basic:
        all_vars.update({"GHOSTPROB": F.GHOSTPROB})
        all_vars.update({"ISMUON": F.ISMUON})
        all_vars.update({"INMUON": F.INMUON})
        all_vars.update({"INECAL": F.INECAL})
        all_vars.update({"INHCAL": F.INHCAL})
        all_vars.update({"HASBREM": F.HASBREM})
        all_vars.update({"BREMENERGY": F.BREMENERGY})
        all_vars.update({"BREMBENDCORR": F.BREMBENDCORR})
        all_vars.update({"BREMPIDE": F.BREMPIDE})
        all_vars.update({"ECALPIDE": F.ECALPIDE})
        all_vars.update({"ECALPIDMU": F.ECALPIDMU})
        all_vars.update({"HCALPIDE": F.HCALPIDE})
        all_vars.update({"HCALPIDMU": F.HCALPIDMU})
        all_vars.update({"ELECTRONSHOWEREOP": F.ELECTRONSHOWEREOP})
        all_vars.update({"CLUSTERMATCH": F.CLUSTERMATCH_CHI2})
        all_vars.update({"ELECTRONMATCH": F.ELECTRONMATCH_CHI2})
        all_vars.update({"BREMHYPOMATCH": F.BREMHYPOMATCH_CHI2})
        all_vars.update({"ELECTRONENERGY": F.ELECTRONENERGY})
        all_vars.update({"BREMHYPOENERGY": F.BREMHYPOENERGY})
        all_vars.update({"BREMHYPODELTAX": F.BREMHYPODELTAX})
        all_vars.update({"ELECTRONID": F.ELECTRONID})
        all_vars.update({"HCALEOP": F.HCALEOP})
        # Note: the observables for the two functors below are (TRACK_MOM_X, TRACK_MOM_Y, TRACK_MOM_Z})
        # and (TRACK_POS_CLOSEST_TO_BEAM_X, TRACK_POS_CLOSEST_TO_BEAM_Y, TRACK_POS_CLOSEST_TO_BEAM_Z),
        # which is why the trailing underscore in the name is added i.e. "TRACK_MOM_" and "TRACK_POS_CLOSEST_TO_BEAM_"
        all_vars.update({"TRACK_MOM_": F.TRACK_MOMVEC})
        all_vars.update({"TRACK_POS_CLOSESTTOBEAM_": F.TRACK_POSVEC_CLOSESTTOBEAM})
        
        all_vars.update({"IS_ABS_ID_pi": F.IS_ABS_ID("pi+")})
        all_vars.update({"IS_ID_pi": F.IS_ID("pi-")})
        all_vars.update({"PDG_MASS_pi": F.PDG_MASS("pi+")})
        all_vars.update({"SIGNED_DELTA_MASS_pi": F.SIGNED_DELTA_MASS("pi+")})
        all_vars.update({"ABS_DELTA_MASS_pi": F.ABS_DELTA_MASS("pi+")})
        all_vars.update({"IS_NOT_H": F.IS_NOT_H})
        all_vars.update({"IS_PHOTON": F.IS_PHOTON})

    all_vars.update({"MASS": F.MASS})

    if comp:
        all_vars.update({"MAXPT": F.MAX(F.PT)})
        all_vars.update({"MAXDOCA": F.MAXSDOCA})
        all_vars.update({"MAXDOCACHI2": F.MAXSDOCACHI2})
        # the above in cut versions.
         
    if comp:
        all_vars.update({"MINPT": F.MIN(F.PT)})
    all_vars.update({"MINIP": F.MINIP(pvs)})
    all_vars.update({"MINIPCHI2": F.MINIPCHI2(pvs)})
    
    if basic:
        all_vars.update({"TRACKPT": F.TRACK_PT})
        all_vars.update({"TRACKHISTORY": F.VALUE_OR(-1) @ F.TRACKHISTORY @ F.TRACK})
        all_vars.update({"QOVERP": F.QOVERP @ F.TRACK})
        all_vars.update({"NDOF": F.VALUE_OR(-1) @ F.NDOF @ F.TRACK})
        all_vars.update({"NFTHITS": F.VALUE_OR(-1) @ F.NFTHITS @ F.TRACK})
        all_vars.update({"NHITS": F.VALUE_OR(-1) @ F.NHITS @ F.TRACK})
        all_vars.update({"NUTHITS": F.VALUE_OR(-1) @ F.NUTHITS @ F.TRACK})
        all_vars.update({"NVPHITS": F.VALUE_OR(-1) @ F.NVPHITS @ F.TRACK})
        all_vars.update({"TRACKHASVELO": F.VALUE_OR(-1) @ F.TRACKHASVELO @ F.TRACK})
        all_vars.update({"TRACKHASUT": F.VALUE_OR(-1) @ F.TRACKHASUT @ F.TRACK})
         
    all_vars.update({"OBJECT_KEY": F.OBJECT_KEY})

    all_vars.update({"PHI": F.PHI})
    
    all_vars.update({"ABS_PX": F.ABS @ F.PX})
    
    all_vars.update({"REFERENCEPOINT_X": F.REFERENCEPOINT_X})
    all_vars.update({"REFERENCEPOINT_Y": F.REFERENCEPOINT_Y})
    all_vars.update({"REFERENCEPOINT_Z": F.REFERENCEPOINT_Z})

    if comp:
        all_vars.update({"SDOCA": F.SDOCA(1, 2)})
        all_vars.update({"SDOCACHI2": F.SDOCACHI2(1, 2)})
    if basic:
        all_vars.update({"SHOWER_SHAPE": F.CALO_NEUTRAL_SHOWER_SHAPE})
        
    if comp:
        all_vars.update({"SUBCOMB12_MM": F.SUBCOMB(Functor=F.MASS, Indices=(1, 2))})
        all_vars.update({"SUMPT": F.SUM(F.PT)})
        
    if basic:
        all_vars.update({"TX": F.TX})
        all_vars.update({"TY": F.TY})
        
    print(f"### For {ptype} returning variables {all_vars.functor_dict.keys()}")
    return all_vars


def event_variables(PVs, ODIN, decreports, lines):
    """
    event variables
    """
     
    evt_vars = FunctorCollection({})
    evt_vars += FC.EventInfo()
    
    evt_vars += FC.SelectionInfo(selection_type="Hlt2", trigger_lines=lines)

    if decreports:                                                                                                                       
        evt_vars.update(
            {
                "DECISIONS": F.DECISIONS(
                    Lines=[bd2dsk_line + "Decision"], DecReports=decreports
                )
            }
        )
        evt_vars.update(
            {
                "DECREPORTS_FILTER": F.DECREPORTS_FILTER(
                    Lines=[bd2dsk_line + "Decision"], DecReports=decreports
                )
            }
        )
         
    if ODIN:
        evt_vars.update({"EVENTTYPE": F.EVENTTYPE(ODIN)})

    evt_vars.update({"PV_SIZE": F.SIZE(PVs)})
    
    if decreports:
        evt_vars.update({"TCK": F.TCK(decreports)})
         
    print(f"### For event returning variables {evt_vars.functor_dict.keys()}")
    return evt_vars


def template(decay_descriptor, line_name, isturbo, Hlt2_decisions):

    evtpath_prefix = "/Event/Spruce/"
    if isturbo:
        evtpath_prefix = "/Event/HLT2/"

    InputLine_data = get_particles(evtpath_prefix + f"{line_name}/Particles")

    pvs = get_pvs()

    Hlt1_decisions = [ 
        'Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision', 'Hlt1D2KKDecision',
        'Hlt1D2KPiDecision', 'Hlt1D2PiPiDecision',
        'Hlt1DiMuonHighMassDecision', 'Hlt1DiMuonLowMassDecision',
        'Hlt1DiMuonSoftDecision',
        'Hlt1KsToPiPiDecision', 'Hlt1LowPtMuonDecision',
        'Hlt1LowPtDiMuonDecision', 'Hlt1SingleHighPtMuonDecision',
        'Hlt1TrackMuonMVADecision', "Hlt1DiMuonNoIP_SSDecision",
        "Hlt1DiMuonDrellYan_VLowMassDecision",
        "Hlt1DiMuonDrellYan_VLowMass_SSDecision",
        "Hlt1DiMuonDrellYanDecision",
        "Hlt1DiMuonDrellYan_SSDecision",
        "Hlt1DetJpsiToMuMuPosTagLineDecision",
        "Hlt1DetJpsiToMuMuNegTagLineDecision",
        "Hlt1TrackElectronMVADecision",
        "Hlt1SingleHighPtElectronDecision",
        "Hlt1DiElectronDisplacedDecision",
        "Hlt1SingleHighEtDecision",
        "Hlt1DiPhotonHighMassDecision",
        "Hlt1Pi02GammaGammaDecision",
        "Hlt1DiElectronHighMass_SSDecision",
        "Hlt1DiElectronHighMassDecision",
        "Hlt1DiMuonNoIPDecision",
     ]

    composite_variables = FunctorCollection({
        "END_VX_ERR":F.SQRT @ F.CALL(0,0) @ F.POS_COV_MATRIX @ F.ENDVERTEX,
        "END_VY_ERR":F.SQRT @ F.CALL(1,1) @ F.POS_COV_MATRIX @ F.ENDVERTEX,
        "END_VZ_ERR":F.SQRT @ F.CALL(2,2) @ F.POS_COV_MATRIX @ F.ENDVERTEX,
        "BPVX_ERR": F.SQRT @ F.CALL(0,0) @ F.POS_COV_MATRIX @ F.BPV(pvs),
        "BPVY_ERR": F.SQRT @ F.CALL(1,1) @ F.POS_COV_MATRIX @ F.BPV(pvs),
        "BPVZ_ERR": F.SQRT @ F.CALL(2,2) @ F.POS_COV_MATRIX @ F.BPV(pvs),
        "PERR": F.SQRT @ F.PERR2,
        "PXERR": F.SQRT @ F.CALL(0,0) @ F.THREE_MOM_COV_MATRIX,
        "PYERR": F.SQRT @ F.CALL(1,1) @ F.THREE_MOM_COV_MATRIX,
        "PZERR": F.SQRT @ F.CALL(2,2) @ F.THREE_MOM_COV_MATRIX,
    })

    composite_variables += HltTisTos( selection_type="Hlt1", trigger_lines=Hlt1_decisions, data=InputLine_data)
    if not isturbo:
        composite_variables += HltTisTos( selection_type="Hlt2", trigger_lines=Hlt2_decisions, data=InputLine_data)

    daughter_variables = FunctorCollection({
        "PERR": F.SQRT @ F.PERR2,
        "PZERR": F.SQRT @ F.CALL(2,2) @ F.THREE_MOM_COV_MATRIX,
    })

    #define event level variables
    odin = get_odin()
    decreports = None
    rec_sum=get_rec_summary()
    event_info = event_variables(pvs, odin, decreports, [line_name]) + FunctorCollection({
        "nPVs": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nPVs"),
        "nTTracks": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nTTracks"),
        "nLongTracks": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nLongTracks"),
        "nDownstreamTracks": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nDownstreamTracks"),
        "nUpstreamTracks": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nUpstreamTracks"),
        "nVeloTracks": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nVeloTracks"),
        "nBackTracks": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nBackTracks"),
        "nRich1Hits": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nRich1Hits"),
        "nRich2Hits": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nRich2Hits"),
        "nVPClusters": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nVPClusters"),
        "nFTClusters": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nFTClusters"),
        "eCalTot": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"eCalTot"),
        "hCalTot": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"hCalTot"),
        "nEcalClusters": F.VALUE_OR(-1) @F.RECSUMMARY_INFO(rec_sum,"nEcalClusters"),
        "ALLPVX": F.ALLPVX(pvs),
        "ALLPVY": F.ALLPVY(pvs),
        "ALLPVZ": F.ALLPVZ(pvs),
    })

    event_info += FC.SelectionInfo(
        selection_type="Hlt1", trigger_lines=Hlt1_decisions
    )

    variables = {
        "Bp":      composite_variables + all_variables(pvs, None, _composite),
        "Ds":      composite_variables + all_variables(pvs, None, _composite),
        "Ds_Km":   daughter_variables + all_variables(pvs, None, _basic),
        "Ds_Kp":   daughter_variables + all_variables(pvs, None, _basic), 
        "Ds_pi":   daughter_variables + all_variables(pvs, None, _basic), 
        "Bp_p":    daughter_variables + all_variables(pvs, None, _basic),
        "Bp_pbar": daughter_variables + all_variables(pvs, None, _basic), 
    }

    #define FunTuple instance
    my_tuple = Funtuple(
        name=line_name,
        tuple_name="DecayTree",
        fields=decay_descriptor,
        variables=variables,
        event_variables=event_info,
        store_multiple_cand_info = True,
        inputs=InputLine_data)

    return my_tuple

def line_prefilter(line_name):
    return create_lines_filter(name=f"HLT_PASS{line_name}", lines=[line_name])
