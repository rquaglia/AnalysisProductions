from .tupling_maker import template, line_prefilter
from DaVinci import Options, make_config

def main(options: Options):
    decay_descriptor = {
        "Bp"      : "^([ B+ -> (D_s+ -> K- K+ pi+) p+ p~- ]CC)",
        "Ds"      :  "[ B+ -> ^(D_s+ -> K- K+ pi+) p+ p~- ]CC",
        "Ds_Km"   :  "[ B+ -> (D_s+ -> ^K- K+ pi+) p+ p~- ]CC",
        "Ds_Kp"   :  "[ B+ -> (D_s+ -> K- ^K+ pi+) p+ p~- ]CC",
        "Ds_pi"   :  "[ B+ -> (D_s+ -> K- K+ ^pi+) p+ p~- ]CC",
        "Bp_p"    :  "[ B+ -> (D_s+ -> K- K+ pi+) ^p+ p~- ]CC",
        "Bp_pbar" :  "[ B+ -> (D_s+ -> K- K+ pi+) p+ ^p~- ]CC",
    }
    line_name = 'Hlt2BandQ_BuToDspPPbar'
    my_tuple  = template(decay_descriptor, line_name, True, [])
    my_filter = line_prefilter(line_name)
    return make_config(options, [my_filter, my_tuple]) 
